# About

JMeterJRDWriterPlugin will be a plugin for JMeter which allows you to write test results to a JRD (Java RRD) file instead of to the xml based .JTL files which the built in results writers use.

# Building

Well, nothing yet since there is zero code in here.

# Running

Same as the above.

# [Further documentation](https://bitbucket.org/omadawn/jmeterjrdwriterplugin/wiki/Home)

Complete documentation will live under the wiki link above.
